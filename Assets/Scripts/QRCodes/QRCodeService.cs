﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UniRx;
using UnityEngine;
using Zenject;
using ZXing;
using ZXing.QrCode;

namespace RPS.QRCodes
{
    public interface IQRCodeService
    {
        /// <summary>
        /// This activates and deactivates the camera. The camera takes a lot of procession power so only activate when necessary
        /// </summary>
        bool CamIsActive { get; set; }

        /// <summary>
        /// The camera texture. This only works if CamIsActive is true
        /// </summary>
        WebCamTexture CamTexture { get; }

        /// <summary>
        /// Returns the value read by the camera. This only works if CamIsActive is true
        /// </summary>
        IObservable<string> ObservedCode { get; }

        /// <summary>
        /// Generates the QR code texture. When the code is observed, it will return the value in the message
        /// </summary>
        /// <returns>The code texture</returns>
        /// <param name="message">The message embedded in the code.</param>
        Texture GenerateCode(string message);
    }

    // Note much of this code came from here https://medium.com/@adrian.n/reading-and-generating-qr-codes-with-c-in-unity-3d-the-easy-way-a25e1d85ba51
    public class QRCodeService : MonoInstaller, IQRCodeService
    {
        private Color32[] webcamBuffer = new Color32[0];
        public WebCamTexture CamTexture { get; private set; }

        private Subject<string> observedCode = new Subject<string>();
        public IObservable<string> ObservedCode => observedCode;

        private bool camIsActive = false;
        public bool CamIsActive
        {
            get => camIsActive;
            set
            {
                if (value)
                {
                    if (CamTexture == null)
                    {
                        CamTexture = new WebCamTexture();
                        CamTexture.requestedHeight = Screen.height;
                        CamTexture.requestedWidth = Screen.width;

                        CamTexture?.Play();
                    }
                    else
                        CamTexture.Play();
                }
                else
                {
                    CamTexture?.Stop();
                    CamTexture = null;
                }
                camIsActive = value;
            }
        }

        private void Start()
        {
            bool threadIsActive = false;

            Observable.EveryUpdate()
                .SkipWhile(_ => threadIsActive)
                .Repeat()
                .TakeUntilDestroy(this)
                .Where(_ => camIsActive)
                .Subscribe(_ =>
                {
                    IBarcodeReader barcodeReader = new BarcodeReader();

                    // Reset the buffer if it is not the right size
                    if (webcamBuffer.Length != CamTexture.width * CamTexture.height)
                        webcamBuffer = CamTexture.GetPixels32();
                    else
                        CamTexture.GetPixels32(webcamBuffer);


                    Result result = null;
                    var width = CamTexture.width;
                    var height = CamTexture.height;
                    threadIsActive = true;
                    // Put the decoding on another thread
                    var thread = new Thread(() =>
                    {
                        // TODO: Shrink the texture
                        result = barcodeReader.Decode(webcamBuffer, width, height);
                    });
                    thread.Start();

                    Observable.EveryUpdate()
                        .First(__ => !thread.IsAlive)
                        .Subscribe(__ =>
                        {
                            threadIsActive = false;
                            if (result != null)
                                observedCode.OnNext(result.Text);
                        });
                });
        }

        public Texture GenerateCode(string message)
        {
            var encoded = new Texture2D(256, 256);
            var color32 = Encode(message, encoded.width, encoded.height);
            encoded.SetPixels32(color32);
            encoded.filterMode = FilterMode.Point;
            encoded.Apply();
            return encoded;
        }

        private Color32[] Encode(string textForEncoding, int width, int height)
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Height = height,
                    Width = width
                }
            };
            return writer.Write(textForEncoding);
        }

        public override void InstallBindings()
        {
            Container.Bind<IQRCodeService>()
                .To<QRCodeService>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}
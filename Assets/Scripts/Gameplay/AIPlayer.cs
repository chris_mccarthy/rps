﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPS.UI;
using UniRx;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace RPS.Gameplay
{
    public class AIPlayer : MonoBehaviour
    {
        [SerializeField] int playerId = 1;

        [Inject] IGameController gameController;
        [Inject] IDialogText dialogText;

        private void Start()
        {
            gameController.OnLaunch
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    Observable.Timer(TimeSpan.FromSeconds(Random.Range(3, 6)))
                        .TakeUntilDestroy(this)
                        .Subscribe(__ =>
                        {
                            // TODO: Add localization
                            dialogText.SetText("Player " + playerId + " has played!");

                            var enumValues = Enum.GetValues(typeof(ShapeType));
                            var randomShape = (ShapeType)enumValues.GetValue(Mathf.FloorToInt(enumValues.Length * Random.value));

                            gameController.SetPlayerState(playerId, new PlayerState
                            {
                                hasPlayed = true,
                                shape = randomShape
                            });
                        });

                });
        }
    }
}
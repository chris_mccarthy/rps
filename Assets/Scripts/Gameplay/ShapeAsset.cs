﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPS.Gameplay
{
    /// <summary>
    /// The shapes played by players in gameplay. ie rock paper or scissors
    /// </summary>
    public enum ShapeType
    {
        Rock,
        Paper,
        Scissors
    }

    /// <summary>
    /// A collection of assets associated with a shape
    /// </summary>
    [CreateAssetMenu(fileName = "ShapeAsset", menuName = "Data/ShapeAsset", order = 1)]
    public class ShapeAsset : ScriptableObject
    {
        public ShapeType shape;
        public Texture texture;
        public ShapeType[] defeatedShapes;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using PaperPlaneTools;
using RPS.Backend;
using RPS.UI;
using UniRx;
using UnityEngine;
using Zenject;

namespace RPS.Gameplay
{
    public interface IBetController
    {
        IReadOnlyReactiveProperty<int> MaxBet { get; }
        IReadOnlyReactiveProperty<int> Bet { get; }
        void PlaceBet(int bet);
    }

    public interface IP2pPlayer
    {
        void StartMatch(string opponentId);
    }

    // TODO: this should be seperated into two classes, p2p player and bet controller
    public class P2pPlayer : MonoBehaviour, IP2pPlayer, IBetController
    {
        // HACK: This is a last minute addition for the pc standalone build
        [SerializeField] private AlertUnityUIAdapter alertAdapter;

        [System.Serializable]
        private class PlayShapeMessage
        {
            public ShapeType shapeType;
        }

        [System.Serializable]
        private class StartGameMessage
        {
            public string opponentId;
            public int maxBet;
        }

        [System.Serializable]
        private class BetMessage
        {
            public int bet;
        }

        [Inject] IP2pController p2PController;
        [Inject] IGameController gameController;
        [Inject] ILoginController loginController;
        [Inject] ICurrencyRepo currencyRepo;
        [Inject] IUIRootController uiRootController;

        string opponentId;

        const string PLAY_SHAPE_HEADER = "PlayShape";
        const string START_GAME_HEADER = "StartGame";
        const string PLACE_BET_HEADER = "PlaceBet";
        const string AGREE_TO_BET_HEADER = "AgreeToBet";
        const string SEND_MAX_BET_HEADER = "SendMaxBet";

        ReactiveProperty<int> maxBet = new ReactiveProperty<int>();
        public IReadOnlyReactiveProperty<int> MaxBet => maxBet;

        ReactiveProperty<int> bet = new ReactiveProperty<int>();
        public IReadOnlyReactiveProperty<int> Bet => bet;

        int purposedBet;

        private void Start()
        {
            // If you receive a play shape message, play that shape
            p2PController.ObserveMessage<PlayShapeMessage>(PLAY_SHAPE_HEADER)
                .TakeUntilDestroy(this)
                .Subscribe(message => gameController.SetPlayerState(1, new PlayerState
                {
                    hasPlayed = true,
                    shape = message.shapeType
                }));

            // If you receive a start game message, go to the place bet screen and tell the player who started the game what your maximum bet is
            p2PController.ObserveMessage<StartGameMessage>(START_GAME_HEADER)
                .TakeUntilDestroy(this)
                .Subscribe(message =>
                {
                    this.opponentId = message.opponentId;
                    maxBet.Value = Mathf.Min(message.maxBet, currencyRepo.GetCurrency("PT").Value);
                    // Open Bet screen
                    uiRootController.OpenRoot("Bet");

                    p2PController.SendMessage(opponentId, SEND_MAX_BET_HEADER, new BetMessage
                    {
                        bet = currencyRepo.GetCurrency("PT").Value
                    });
                });

            // If you receive a place bet message, show an alert to confirm if they want to play with that bet
            p2PController.ObserveMessage<BetMessage>(PLACE_BET_HEADER)
                .TakeUntilDestroy(this)
                .Subscribe(message =>
                {
                    purposedBet = message.bet;
                    // Show some sort of alert purposing to bet
                    new Alert("Bet placed!", "Opponent placed bet of " + message.bet)
                        .SetPositiveButton("Yes", () =>
                        {
                            p2PController.SendMessage(opponentId, AGREE_TO_BET_HEADER, new BetMessage
                            {
                                bet = purposedBet
                            });

                            bet.Value = purposedBet;

                            uiRootController.OpenRoot("Game");
                            gameController.Reset();
                        })
                        .SetNegativeButton("No")
#if UNITY_STANDALONE
                        .SetAdapter(alertAdapter)
#endif
                        .Show();
                });

            // If you receive an agree to bet message, launch the game
            p2PController.ObserveMessage<BetMessage>(AGREE_TO_BET_HEADER)
                .TakeUntilDestroy(this)
                .Subscribe(message =>
                {
                    // Set bet
                    bet.Value = purposedBet;
                    // Launch game
                    uiRootController.OpenRoot("Game");
                    gameController.Reset();
                });

            // If you receive your opponent's max bet, set the value
            p2PController.ObserveMessage<BetMessage>(SEND_MAX_BET_HEADER)
                .TakeUntilDestroy(this)
                .Subscribe(message =>
                {
                    maxBet.Value = Mathf.Min(message.bet, currencyRepo.GetCurrency("PT").Value);
                });

            // When the player plays a shape, send it
            gameController.GetPlayerState(0)
                .TakeUntilDestroy(this)
                .Where(state => state.hasPlayed)
                .Subscribe(state => p2PController.SendMessage(opponentId, PLAY_SHAPE_HEADER, new PlayShapeMessage
                {
                    shapeType = state.shape
                }));
        }

        public void PlaceBet(int bet)
        {
            purposedBet = bet;

            p2PController.SendMessage(opponentId, PLACE_BET_HEADER, new BetMessage
            {
                bet = bet
            });
        }

        public void StartMatch(string opponentId)
        {
            this.opponentId = opponentId;

            p2PController.SendMessage(opponentId, START_GAME_HEADER, new StartGameMessage
            {
                opponentId = loginController.UserId.Value,
                maxBet = currencyRepo.GetCurrency("PT").Value
            });

            uiRootController.OpenRoot("Bet");
        }
    }
}
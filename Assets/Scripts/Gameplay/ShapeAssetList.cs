﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace RPS.Gameplay
{
    /// <summary>
    /// Use this to get assets associated with a shape
    /// </summary>
    public interface IShapeAssetList
    {
        ShapeAsset GetShape(ShapeType shape);
    }

    public class ShapeAssetList : MonoInstaller, IShapeAssetList
    {
        [SerializeField]
        ShapeAsset[] shapeAssets;

        public ShapeAsset GetShape(ShapeType shape)
        {
            return shapeAssets.FirstOrDefault(asset => asset.shape == shape);
        }

        public override void InstallBindings()
        {
            Container.Bind<IShapeAssetList>()
                .To<ShapeAssetList>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}
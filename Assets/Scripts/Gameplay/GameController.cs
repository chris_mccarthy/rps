﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RPS.Backend;
using RPS.UI;
using UniRx;
using UnityEngine;
using Zenject;

namespace RPS.Gameplay
{
    /// <summary>
    /// This is used to track the players state in the game
    /// </summary>
    public interface IGameController
    {
        /// <summary>
        /// Gets the state of the player.
        /// </summary>
        /// <returns>A reactive property of a player's current state</returns>
        /// <param name="playerId">Player id. 0 indicates local player</param>
        IReadOnlyReactiveProperty<PlayerState> GetPlayerState(int playerId);

        /// <summary>
        /// Sets the local player's state
        /// </summary>
        /// <param name="playerState">Player state.</param>
        void SetPlayerState(int playerId, PlayerState playerState);

        IObservable<Unit> OnLaunch { get; }

        /// <summary>
        /// An event called when all the various player's choice of shape is revealed
        /// </summary>
        IObservable<Unit> OnReveal { get; }

        /// <summary>
        /// An event called when the game is complete
        /// </summary>
        IObservable<GameOutcome> OnGameComplete { get; }

        void Reset();
    }

    /// <summary>
    /// The state of the player in the game's progression
    /// </summary>
    public class PlayerState
    {
        public bool hasPlayed;
        public ShapeType shape;
    }

    public class GameOutcome
    {
        public bool hasWinner;
        public int winnerId;
    }

    public class GameController : MonoBehaviour, IGameController
    {
        [Inject] IShapeAssetList shapeAssetList;
        [Inject] IDialogText dialogText;
        [Inject] IBetController betController;
        [Inject] ICurrencyRepo currencyRepo;

        Dictionary<int, ReactiveProperty<PlayerState>> playerStates = new Dictionary<int, ReactiveProperty<PlayerState>>();

        private Subject<Unit> onLaunch = new Subject<Unit>();
        public IObservable<Unit> OnLaunch => onLaunch;

        private Subject<Unit> onReveal = new Subject<Unit>();
        public IObservable<Unit> OnReveal => onReveal;

        private Subject<GameOutcome> onGameComplete = new Subject<GameOutcome>();
        public IObservable<GameOutcome> OnGameComplete => onGameComplete;

        public IReadOnlyReactiveProperty<PlayerState> GetPlayerState(int playerId)
        {
            if (!playerStates.ContainsKey(playerId))
                playerStates[playerId] = new ReactiveProperty<PlayerState>(new PlayerState());

            return playerStates[playerId];
        }

        public void SetPlayerState(int playerId, PlayerState playerState)
        {
            if (playerStates.ContainsKey(playerId))
                playerStates[playerId].Value = playerState;
            else
                playerStates[playerId] = new ReactiveProperty<PlayerState>(playerState);

            CheckForEndGame();
        }

        private void CheckForEndGame()
        {
            // If there are no players who have not played
            var allPlayersPlayed = !playerStates.Values
                .Any(state => !state.Value.hasPlayed);

            if (allPlayersPlayed)
            {
                onReveal.OnNext(Unit.Default);

                // Find a winners by checking if any other players played a shape that would defeat their shape
                var winners = playerStates
                    .Where(kvp =>
                    {
                        var playerState = kvp.Value;

                        var otherPlayerShapes = playerStates.Values
                            .Where(otherState => otherState != playerState)
                            .Select(otherState => shapeAssetList.GetShape(otherState.Value.shape));

                        var isDefeated = otherPlayerShapes
                            .Any(shape => shape.defeatedShapes.Contains(playerState.Value.shape));

                        return !isDefeated;
                    });

                // Though the game logic is set up to accommedate more than one player, this will not be utilized
                var gameOutcome = new GameOutcome
                {
                    hasWinner = winners.Count() == 1,
                    winnerId = winners.FirstOrDefault().Key
                };

                onGameComplete.OnNext(gameOutcome);

                var bet = betController.Bet.Value;
                var localPlayerWon = gameOutcome.winnerId == 0;
                // TODO: Add localization
                dialogText.SetText((!gameOutcome.hasWinner) ? "Draw!" : localPlayerWon ? "You Win " + bet + "!" : "You Lose " + bet + "!");

                if (gameOutcome.hasWinner)
                {
                    var newCurrency = currencyRepo.GetCurrency("PT").Value + (localPlayerWon ? bet : -bet);
                    currencyRepo.SetCurrency("PT", newCurrency);
                }
            }
        }

        public void Reset()
        {
            foreach (var s in playerStates.Values)
            {
                s.Value = new PlayerState();
            }

            onLaunch.OnNext(Unit.Default);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using RPS.UI;
using UnityEngine;
using Zenject;

namespace RPS.Gameplay
{
    public class GameplayInstaller : MonoInstaller
    {
        [SerializeField] DialogText text;
        [SerializeField] GameController gameController;
        [SerializeField] P2pPlayer p2pPlayer;
        [SerializeField] UIRootController uiRootController;

        public override void InstallBindings()
        {
            Container.Bind<IGameController>()
                .FromInstance(gameController)
                .AsSingle();

            Container.Bind<IDialogText>()
                .FromInstance(text)
                .AsSingle();

            Container.Bind<IP2pPlayer>()
                .FromInstance(p2pPlayer)
                .AsSingle();

            Container.Bind<IBetController>()
                .FromInstance(p2pPlayer)
                .AsSingle();

            Container.Bind<IUIRootController>()
                .FromInstance(uiRootController)
                .AsSingle();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using RPS.Gameplay;
using RPS.QRCodes;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.UI
{
    public class CodeReadingUI : MonoBehaviour
    {
        [SerializeField] RawImage image;
        [SerializeField] GameObject root;
        [SerializeField] GameObject activatedRoot;
        [SerializeField] AspectRatioFitter aspectRatioFitter;

        [Inject] IQRCodeService qrCodeService;
        [Inject] IGameController gameController;
        [Inject] IP2pPlayer p2PPlayer;

        private void OnEnable()
        {
            qrCodeService.CamIsActive = true;

            image.texture = qrCodeService.CamTexture;

            Observable.EveryUpdate()
                .TakeUntilDisable(this)
                .Select(_ => qrCodeService.CamTexture)
                .DistinctUntilChanged(texture => new Vector2(texture.width, texture.height))
                .Subscribe(texture =>
                {
                    var camTextureRotation = -qrCodeService.CamTexture.videoRotationAngle;
                    image.transform.rotation = Quaternion.Euler(0, 0, camTextureRotation);

                    var isSideways = Mathf.Abs(camTextureRotation) > 45 && Mathf.Abs(camTextureRotation) < 135;
                    var aspectRatio = (float)texture.width / (float)texture.height;

                    aspectRatioFitter.aspectMode = isSideways ? AspectRatioFitter.AspectMode.HeightControlsWidth : AspectRatioFitter.AspectMode.WidthControlsHeight;
                    aspectRatioFitter.aspectRatio = aspectRatio;
                });

            qrCodeService.ObservedCode
                .TakeUntilDisable(this)
                .Subscribe(opponentId =>
                {
                    root.SetActive(false);
                    activatedRoot.SetActive(true);

                    p2PPlayer.StartMatch(opponentId);
                    Debug.Log("Read code: " + opponentId);
                });
        }

        private void OnDisable()
        {
            qrCodeService.CamIsActive = false;
        }
    }
}
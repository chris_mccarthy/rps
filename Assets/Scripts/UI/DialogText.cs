﻿using System.Collections;
using System.Collections.Generic;
using RPS.Gameplay;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.UI
{
    public interface IDialogText
    {
        void SetText(string text);
    }

    public class DialogText : MonoBehaviour, IDialogText
    {
        [SerializeField] Text text;

        [Inject] IGameController gameController;

        private void Start()
        {
            gameController.OnLaunch
                .TakeUntilDestroy(this)
                .Subscribe(_ => text.text = string.Empty);
        }

        public void SetText(string text)
        {
            this.text.text = text;
        }
    }
}
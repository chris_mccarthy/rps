﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.Gameplay
{
    public class PlayerShapeIcon : MonoBehaviour
    {
        [SerializeField] int representedPlayer;
        [SerializeField] bool maskChoice;
        [SerializeField] Texture unknownImage;
        [SerializeField] GameObject visualRoot;
        [SerializeField] RawImage image;

        [Inject] IGameController gameController;
        [Inject] IShapeAssetList shapeAssetList;

        private void Start()
        {
            // If the player has played, set the icon active
            gameController.GetPlayerState(representedPlayer)
                .TakeUntilDestroy(this)
                .Select(playerState => playerState.hasPlayed)
                .DistinctUntilChanged()
                .Subscribe(visualRoot.SetActive);

            // Set the shape icon to what they chose or mask it
            gameController.GetPlayerState(representedPlayer)
                .TakeUntilDestroy(this)
                .Select(playerState => playerState.shape)
                .DistinctUntilChanged()
                .Select(shapeAssetList.GetShape)
                .Select(shapeAsset => (maskChoice) ? unknownImage : shapeAsset.texture)
                .Subscribe(texture => image.texture = texture);

            // When revealed, set the shape icon to what was chosen
            gameController.OnReveal
                .TakeUntilDestroy(this)
                .Select(_ => shapeAssetList.GetShape(gameController.GetPlayerState(representedPlayer).Value.shape).texture)
                .Subscribe(texture => image.texture = texture);
        }
    }
}
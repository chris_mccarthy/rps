﻿using System.Collections;
using System.Collections.Generic;
using RPS.Gameplay;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;

namespace RPS.UI
{
    public class ShapeButton : MonoBehaviour
    {
        [SerializeField] Button button;
        [SerializeField] RawImage image;
        [SerializeField] ShapeType shape;

        [Inject] IGameController gameController;
        [Inject] IShapeAssetList shapeAssetList;

        private void Start()
        {
            button.interactable = false;

            var shapeAsset = shapeAssetList.GetShape(shape);
            image.texture = shapeAsset.texture;

            button.OnClickAsObservable()
                .TakeUntilDestroy(this)
                .Subscribe(_ =>
                {
                    gameController.SetPlayerState(0, new PlayerState
                    {
                        hasPlayed = true,
                        shape = shape
                    });
                });

            gameController.GetPlayerState(0)
                .TakeUntilDestroy(this)
                .Select(state => state.hasPlayed)
                .CombineLatest(gameController.OnLaunch, (hasPlayed, hasLaunched) => hasPlayed)
                .DistinctUntilChanged()
                .Subscribe(hasPlayed => button.interactable = !hasPlayed);

            gameController.OnGameComplete
                .TakeUntilDestroy(this)
                .Subscribe(_ => button.interactable = false);
        }
    }
}
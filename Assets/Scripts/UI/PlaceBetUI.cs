﻿using System.Collections;
using System.Collections.Generic;
using RPS.Backend;
using RPS.Gameplay;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.UI
{
    public class PlaceBetUI : MonoBehaviour
    {
        [SerializeField] Text dialogText;
        [SerializeField] Button button;
        [SerializeField] InputField inputField;

        [Inject] IBetController betController;
        [Inject] ICurrencyRepo currencyRepo;

        private void OnEnable()
        {
            // TODO: Add localization
            betController.MaxBet
                .TakeUntilDisable(this)
                .Subscribe(maxBet => dialogText.text = "Place your bet! Max bet " + maxBet);

            button.OnClickAsObservable()
                .TakeUntilDisable(this)
                .Subscribe(_ =>
                {
                    int.TryParse(inputField.text, out int enteredBet);
                    var maxBet = betController.MaxBet.Value;

                    if (enteredBet > maxBet)
                        dialogText.text = "Too high! Max bet " + maxBet;
                    else
                    {
                        dialogText.text = "Placed bet of " + enteredBet + ". Waiting for reply.";
                        betController.PlaceBet(enteredBet);
                    }
                });
        }
    }
}
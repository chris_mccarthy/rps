﻿using System.Collections;
using System.Collections.Generic;
using RPS.Backend;
using RPS.QRCodes;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.UI
{
    public class CodeSendingUI : MonoBehaviour
    {
        [SerializeField] RawImage image;
        [SerializeField] AspectRatioFitter aspectRatioFitter;

        [Inject] IQRCodeService codeService;
        [Inject] ILoginController loginController;

        private void OnEnable()
        {
            loginController.UserId
                .TakeUntilDisable(this)
                .Subscribe(userId =>
                {
                    var texture = codeService.GenerateCode(userId);
                    image.texture = texture;
                    aspectRatioFitter.aspectRatio = (float)texture.width / (float)texture.height;
                });
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPS.UI
{
    public interface IUIRootController
    {
        void OpenRoot(string id);
    }

    public class UIRootController : MonoBehaviour, IUIRootController
    {
        [System.Serializable]
        private class IdRoot
        {
            public string id;
            public GameObject root;
        }

        [SerializeField] IdRoot[] idRoots;
        [SerializeField] GameObject[] allRoots;

        public void OpenRoot(string id)
        {
            foreach (var r in allRoots)
            {
                r.SetActive(false);
            }

            idRoots
                .FirstOrDefault(r => r.id == id)?
                .root.SetActive(true);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using RPS.Backend;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace RPS.UI
{
    public class CurrencyLabel : MonoBehaviour
    {
        [SerializeField] string currencyType = "PT";
        [SerializeField] string currencySuffix = "p";
        [SerializeField] Text text;

        [Inject] ICurrencyRepo currencyRepo;

        private void Start()
        {
            currencyRepo.GetCurrency(currencyType)
                .TakeUntilDestroy(this)
                .Subscribe(currency => text.text = currency + currencySuffix);
        }
    }
}
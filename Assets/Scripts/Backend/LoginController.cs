﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using UniRx;
using UnityEngine;
using Zenject;

namespace RPS.Backend
{
    public interface ILoginController
    {
        IReadOnlyReactiveProperty<bool> IsLoggedIn { get; }
        IReadOnlyReactiveProperty<string> UserId { get; }
    }

    public class LoginController : MonoInstaller, ILoginController
    {
        private ReactiveProperty<string> userId = new ReactiveProperty<string>();
        public IReadOnlyReactiveProperty<string> UserId => userId;

        private ReactiveProperty<bool> isLoggedIn = new ReactiveProperty<bool>();
        public IReadOnlyReactiveProperty<bool> IsLoggedIn => isLoggedIn;

        private void Start()
        {
            PlayFabClientAPI.LoginWithCustomID(
                new PlayFab.ClientModels.LoginWithCustomIDRequest
                {
                    CustomId = SystemInfo.deviceUniqueIdentifier,
                    CreateAccount = true
                },
                results =>
                {
                    userId.Value = results.PlayFabId;
                    isLoggedIn.Value = true;
                    Debug.Log("Logged in annonymously");
                },
                error => Debug.LogError(error.GenerateErrorReport())
            );
        }

        public override void InstallBindings()
        {
            Container.Bind<ILoginController>()
                .To<LoginController>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}
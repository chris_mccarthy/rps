﻿using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Chat;
using SimpleJSON;
using UniRx;
using UnityEngine;
using Zenject;

namespace RPS.Backend
{
    public interface IP2pController
    {
        IObservable<T> ObserveMessage<T>(string header);
        void SendMessage<T>(string receiver, string header, T message);
    }

    public class P2pController : MonoInstaller, IP2pController
    {
        // HACK: This should come from the settings in editor
        [SerializeField] string appId;
        [SerializeField] string versionNumber;
        [SerializeField] bool logEvents;

        [Inject] ILoginController loginController;

        ChatClient chatClient;
        ChatClientBroadcaster chatBroadcaster;
        Dictionary<string, Subject<ChatPrivateMessage>> subscribers = new Dictionary<string, Subject<ChatPrivateMessage>>();

        private void Start()
        {
            // Once we know the username, login using it
            loginController.UserId
                .TakeUntilDestroy(this)
                .Where(userId => !string.IsNullOrEmpty(userId))
                .Subscribe(userId =>
                {
                    chatBroadcaster = new ChatClientBroadcaster();
                    chatClient = new ChatClient(chatBroadcaster);

                    chatBroadcaster.chatClient = chatClient;
                    chatBroadcaster.logEvents = logEvents;

                    chatClient.Connect(appId, versionNumber, new AuthenticationValues(userId));

                    // This needs to be called every update for chat client to work
                    Observable.EveryUpdate()
                        .TakeUntilDestroy(this)
                        .Subscribe(_ => chatClient.Service());

                    // When you receive a message, broadcast it to the subscribers
                    chatBroadcaster.onPrivateMessage
                        .TakeUntilDestroy(this)
                        .Where(message => subscribers.ContainsKey(message.header))
                        .Subscribe(message => subscribers[message.header].OnNext(message));
                });

        }

        public void SendMessage<T>(string receiver, string header, T message)
        {
            Debug.Log("Sending message with header: " + header);

            var rootNode = new JSONObject();

            rootNode["header"] = header;
            rootNode["payload"] = JsonUtility.ToJson(message);

            chatClient.SendPrivateMessage(receiver, rootNode.ToString());
        }

        public IObservable<T> ObserveMessage<T>(string header)
        {
            if (!subscribers.ContainsKey(header))
                subscribers.Add(header, new Subject<ChatPrivateMessage>());

            return subscribers[header]
                .Select(message => JsonUtility.FromJson<T>(message.data));
        }

        public override void InstallBindings()
        {
            Container.Bind<IP2pController>()
                .To<P2pController>()
                .FromInstance(this)
                .AsSingle();
        }
    }

    public class ChatPrivateMessage
    {
        public string sender;
        public string header;
        public JSONNode data;
    }

    public class ChatClientBroadcaster : IChatClientListener
    {
        // TODO: Add logging filtering
        public bool logEvents;
        public ChatClient chatClient;

        public void DebugReturn(DebugLevel level, string message)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: DebugReturn " + message);
        }

        public Subject<ChatDisconnectCause> onDisconnected = new Subject<ChatDisconnectCause>();
        public void OnDisconnected()
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnDisconnected Cause: " + chatClient.DisconnectedCause);
            onDisconnected.OnNext(chatClient.DisconnectedCause);
        }

        public Subject<Unit> onConnected = new Subject<Unit>();
        public void OnConnected()
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnConnected");
            onConnected.OnNext(Unit.Default);
        }

        public Subject<ChatState> onChatStateChange = new Subject<ChatState>();
        public void OnChatStateChange(ChatState state)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnChatStateChange " + state);
            onChatStateChange.OnNext(state);
        }

        public void OnGetMessages(string channelName, string[] senders, object[] messages)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnGetMessages");
        }

        public Subject<ChatPrivateMessage> onPrivateMessage = new Subject<ChatPrivateMessage>();
        public void OnPrivateMessage(string sender, object message, string channelName)
        {
            //HACK: for some reason all private messages get sent to the sender. I dunno but this patch fixes it.
            if (sender == chatClient.UserId)
                return;
            var jsonRoot = JSON.Parse(message.ToString());
            var header = jsonRoot["header"];
            var data = jsonRoot["payload"];

            if (logEvents)
                Debug.Log("IChatClientListener: OnPrivateMessage\nsender: " + sender + "\nheader: " + header + "\ndata: " + data);

            onPrivateMessage.OnNext(new ChatPrivateMessage
            {
                sender = sender,
                header = header,
                data = data,
            });
        }

        public void OnSubscribed(string[] channels, bool[] results)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnSubscribed");
        }

        public void OnUnsubscribed(string[] channels)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnUnsubscribed");
        }

        public Subject<Tuple<string, int>> onStatusUpdate = new Subject<Tuple<string, int>>();
        public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnStatusUpdate");
            onStatusUpdate.OnNext(Tuple.Create(user, status));
        }

        public void OnUserSubscribed(string channel, string user)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnUserSubscribed");
        }

        public void OnUserUnsubscribed(string channel, string user)
        {
            if (logEvents)
                Debug.Log("IChatClientListener: OnUserUnsubscribed");
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UniRx;
using UnityEngine;
using Zenject;

namespace RPS.Backend
{
    public interface ICurrencyRepo
    {
        IReadOnlyReactiveProperty<int> GetCurrency(string code);
        void SetCurrency(string code, int value);
    }

    public class CurrencyRepo : MonoInstaller, ICurrencyRepo
    {
        [Inject] ILoginController loginController;

        Dictionary<string, ReactiveProperty<int>> currencies = new Dictionary<string, ReactiveProperty<int>>();

        private void Start()
        {
            loginController.IsLoggedIn
                .TakeUntilDestroy(this)
                .Where(isLoggedIn => isLoggedIn)
                .Subscribe(_ =>
                {
                    PlayFabClientAPI.GetUserInventory(
                        new GetUserInventoryRequest(),
                        results =>
                        {
                            foreach (var c in results.VirtualCurrency)
                            {
                                if (!currencies.ContainsKey(c.Key))
                                    currencies.Add(c.Key, new ReactiveProperty<int>(c.Value));
                                else
                                    currencies[c.Key].Value = c.Value;
                            }
                        },
                        error => Debug.LogError(error));
                });
        }

        public IReadOnlyReactiveProperty<int> GetCurrency(string code)
        {
            if (!currencies.ContainsKey(code))
                currencies.Add(code, new ReactiveProperty<int>(0));

            return currencies[code];
        }

        public void SetCurrency(string code, int value)
        {
            // HACK: this is an extremely insecure way of handling currency
            var request = new ExecuteCloudScriptRequest
            {
                FunctionName = "setUserVirtualCurrency",
                FunctionParameter = new
                {
                    virtualCurrency = code,
                    amount = value
                },
                GeneratePlayStreamEvent = true
            };

            PlayFabClientAPI.ExecuteCloudScript(request,
                result =>
                {
                    if (result.Error != null)
                    {
                        var error = string.Format("There was error in the Cloud Script function {0}:\n Error Code: {1}\n Message: {2}",
                            result.FunctionName, result.Error.Error, result.Error.Message);

                        Debug.LogError(error);
                    }
                    else
                    {
                        if (!currencies.ContainsKey(code))
                            currencies.Add(code, new ReactiveProperty<int>(value));
                        else
                            currencies[code].Value = value;
                    }

                },
                error => Debug.LogError(error.GenerateErrorReport()));
        }

        public override void InstallBindings()
        {
            Container.Bind<ICurrencyRepo>()
                .FromInstance(this)
                .AsSingle();
        }
    }
}